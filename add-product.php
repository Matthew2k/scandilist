<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-3.5.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
    <script type="text/javascript">
        $("#book_form , #furniture_form").hide();
    </script>
    <div class="addBlock">
        <div class="paddingBlock">
            <div class="widgetBlock widgetMarg">
                <h2 class="heading_text">Add the Products</h2>
                <a href="index.php"><button id="backToListBtn" class="click_button">List all Products</button></a>
            </div>
            <div class="productAddBlock">
                <form action="add-product.php" method="POST">
                <div class="snp_block">
                    <label>SKU:<br><input type="text" class="data_input" id="prod_sku" name="prod_sku"></label><br>
                    <label>Name:<br><input type="text" class="data_input" id="prod_name" name="prod_title"></label><br>
                    <label>Price:<br><input type="text" class="data_input" id="prod_price" name="prod_price"></label><br>
                </div>
                <div class="switcher_block">
                    <label class="switcher_caption">
                        Select Product Type:
                    </label>
                    <select name="switch_form" id="switch_form">
                        <option value="dvd">DVD</option>
                        <option value="books">Book</option>
                        <option value="furniture">Furniture</option>    
                    </select>
                </div>

                <div class="formBlock">
                <div id="dvd_form">
                        <label class="input_caption">Size:<br><input type="text" class="data_input" id="dvd_size" name="dvd_size"></label><br>
                        <p class="attr_caption">
                            Plese write a size in MB like this: 123
                        </p>
                    </div>
                    <div id="books_form" class="non_display">
                        <label class="input_caption">Weight:<br><input type="text" class="data_input" id="book_weight" name="book_weight"></label><br>
                        <p class="attr_caption">
                            Plese write a weight in KG like this: 123
                        </p>
                    </div>
                    <div id="furniture_form" class="non_display">
                        <label class="input_caption">Height:<br><input type="text" class="data_input" id="fur_height" name="fur_height"></label><br>
                        <label class="input_caption">Width:<br><input type="text" class="data_input" id="fur_width" name="fur_width"></label><br>
                        <label class="input_caption">Length:<br><input type="text" class="data_input" id="fur_length" name="fur_length"></label>
                        <p class="attr_caption">
                            Plese write a dimensions in HxWxL format
                        </p>
                    </div>
                </div>
                <button id="submitBtn" class="click_button" name="submit_button" type="Submit">Add</button>
                <button id="resetBtn" class="click_button" name="reset_button" type="Reset">Reset</button>
                </form>
                <?php
                    include ("adding.php");
                ?>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#switch_form").change(function(){
                $("#dvd_form , #books_form , #furniture_form").hide();
                $("#" + $(this).val() + "_form").show();
            });
        });
    </script>
</body>
</html>