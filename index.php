<html>
  <head>
    <meta charset="utf-8">
    <title>Scandiweb Product List</title>
    <link rel="stylesheet" href="css/product.php">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery-3.5.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  </head>
  <body>
      <div class="mainBlock">
        <div class="paddingBlock">
          <div class="widgetBlock">
            <div class="heading_block">
              <h2 class="heading_text">All the Products</h2>
            </div>
            <div class="btn_block">
              <label class="checkAllCaption"><input type="checkbox" id="checkAll"><span class="checkAllCaption">Select All the Products</span></label>
              <button type="button" id="deleteBtn" class="click_button">Delete</button>
              <a href="add-product.php"><button type="button" id="addBtn" class="click_button">Add Product</button></a>
            </div>
        </div>
          <?php
            function checkRow($result){
              if($result->num_rows > 0){
                return true;
              } else {
                return false;
              }
            }

            // Print the product funtions
            include ("printFunctions.php");

            function checkTable($table_name){
              // Connecting the Database with the Page
              $connect_db = new mysqli("localhost","root","","products");
              // Checking connection
              if($connect_db->connect_error){
                die("We didn't setup the connection with the DataBase" . $connect_db->connect_error);
              }
              // Sql Request and Converting data
              // Useing the String passed in function , choose which columns should be taken from the table
              if($table_name == "books"){
                    $sql_request = "SELECT sku, title, price, wght FROM " . $table_name;
                    printBooks($connect_db,$sql_request);
              } elseif($table_name == "dvd"){
                    $sql_request = "SELECT sku, title, price, size FROM " . $table_name;
                    printDVD($connect_db,$sql_request);
              } elseif($table_name == "furniture") {
                    $sql_request = "SELECT sku, title, price, dem FROM " . $table_name;
                    printFurniture($connect_db,$sql_request);
                    }
                  mysqli_close($connect_db); 
                }

            checkTable("furniture");
            checkTable("dvd");
            checkTable("books");
            
            /*
                25.06 -- Code Updates 
                  1. Created database for each product type.
                  2. Found dumb solution for displaykng them - CP the code 3 times which is not good 
                  3. I finally started doing this task!!!!!  :D

                -- Code Fixes for 26.06
                1. Find solution to check which table is going to be used in function to have universal function for all the sql requests UPDATE : DONE
                2. Add Mass delete to the List UPDATE : IN PROGRESS
              */

          ?>
        </div>
      </div>
      <script>

        // Select all the Prodcuts in the List
        $(document).ready(function(){
          $("#checkAll").click(function(){
            if(this.checked){
              $(".checkbox").each(function(){
                this.checked = true;
              });
            }else {
              $(".checkbox").each(function(){
                this.checked = false;
              });
            }
          });
        });

        $("#deleteBtn").click(function(){
          var dataArray = new Array();
          if($("input:checkbox:checked").length > 0){
            $("input:checkbox:checked").each(function(){
              dataArray.push($(this).attr("id"));
              $(this).closest('div').remove();
            });
            sendResponse(dataArray);
          }else {
            alert("No prodcuts were selected to delete");
          }
        });

        function sendResponse(dataArray){
          $.ajax({
              type    : 'post',
              url     : 'delete.php',
              data    : {'data' : dataArray},
              success : function(response){
                          alert(response);
                        },
              error   : function(errResponse){
                          alert(errResponse);
              }
          });
        }

      </script>
  </body>
</html>