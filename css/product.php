<?php
header("Content-type: text/css");


// Statement for product

$inline = 'inline-block';
$min_w = '250px';
$min_h = '100px';
$padding = '10px';
$jus_cont = 'space-around';
$b_radius = '5px';

$font_c = '#9A9A9A';

$center = 'center';
$shadow = '0px 10px 10px 0px rgba(219,219,219,1)';
$shadow_hov = '0px 10px 20px 10px rgba(209,209,209,1)';

$cd_t = '0.3s';

?>

.product_block{
    background-color:#fff;
    margin:20px;
    text-align:<?=$center?>;
    display: <?=$inline?>;
    min-width: <?=$min_w?>;
    min-height: <?=$min_h?>;
    border-radius:<?=$b_radius?>;
    padding:10px;
    box-shadow:<?=$shadow?>;
    color:<?=$font_c?>;
    justify-content: <?=$jus_cont?>;
    transition:<?=$cd_t?>;
  }
  .product_block:hover{
    box-shadow:<?=$shadow_hov?>;
    color:black;
  }