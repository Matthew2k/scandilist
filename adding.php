<?php
    // Connecting the Database with the Page
    $connect_db = new mysqli("localhost","root","","products");
    // Checking connection
    if($connect_db->connect_error){
        die("We didn't setup the connection with the DataBase" . $connect_db->connect_error);
    }

    if(isset($_POST["submit_button"])){

        $prod_type = $_POST["switch_form"];
        $prod_sku = $_POST["prod_sku"];
        $prod_title = $_POST["prod_title"];
        $prod_price = $_POST["prod_price"];
        
        switch ($prod_type){

            case 'dvd' :

                //Check if SKU is a duplicate
                $check_dup = "SELECT sku FROM dvd WHERE sku ='$prod_sku'";
                $result = mysqli_query($connect_db,$check_dup);
                $count_dup = mysqli_num_rows($result);

                // If it is - throw error
                if($count_dup > 0){
                    echo "The SKU code should be unique for each product";
                } else {
                    $dvd_size = $_POST['dvd_size'];
                    $sql_request = "INSERT INTO $prod_type (sku, title, price, size) VALUES ('$prod_sku','$prod_title','$prod_price','$dvd_size')";
                    if(mysqli_query($connect_db,$sql_request)){
                        echo "DVD was Added!";
                    }else {
                        echo "DVD was Not Added!";
                    }
                }
                mysqli_close($connect_db);
            break;

            case 'books' :
                //Check if SKU is a duplicate
                $check_dup = "SELECT sku FROM books WHERE sku ='$prod_sku'";
                $result = mysqli_query($connect_db,$check_dup);
                $count_dup = mysqli_num_rows($result);

                // If it is - throw error
                if($count_dup > 0){
                    echo "The SKU code should be unique for each product type";
                } else {
                    $book_weight = $_POST['book_weight'];
                    $sql_request = "INSERT INTO $prod_type (sku, title, price, wght) VALUES ('$prod_sku','$prod_title','$prod_price','$book_weight')";
                    if(mysqli_query($connect_db,$sql_request)){
                        echo "Book was Added!";
                    }else {
                        echo "Book was Not Added!";
                    }
                }
                mysqli_close($connect_db);
                break;

            case 'furniture' :
                $check_dup = "SELECT sku FROM furniture WHERE sku ='$prod_sku'";
                $result = mysqli_query($connect_db,$check_dup);
                $count_dup = mysqli_num_rows($result);

                // If it is - throw error
                if($count_dup > 0){
                    echo "The SKU code should be unique for each product type";
                } else {
                    $dems = $_POST['fur_height']."x".$_POST['fur_width']."x".$_POST['fur_length'];
                    $sql_request = "INSERT INTO $prod_type (sku, title, price, dem) VALUES ('$prod_sku','$prod_title','$prod_price','$dems')";
                    if(mysqli_query($connect_db,$sql_request)){
                        echo "Furniture was Added!";
                    }else {
                        echo "Furniture was Not Added!";
                    }
                }
                mysqli_close($connect_db);
                break;

            default:
                echo "Something went wrong , sorry";
            break;
        }
    }
?>