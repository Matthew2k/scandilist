<?php
        function printDVD($conn,$request){
              $result = $conn->query($request);
              if(checkRow($result)){
              while($output = $result->fetch_assoc()){
              echo "
              <div class='product_block'>
                <input id='DVD-".$output['sku']."' type='checkbox' class='checkbox'>
                <p>DVD-".$output['sku']."</p>
                <p>".$output['title']."</p>
                <p>$".$output['price']."</p>
                <p>".$output['size']." MB</p>
              </div>
              ";
              }
              } else {
                  echo "Nothing was found";
              }
            }

            function printBooks($conn,$request){
              $result = $conn->query($request);
              if(checkRow($result)){
              while($output = $result->fetch_assoc()){
                echo "
                <div class='product_block'>
                  <input id='BOOK-".$output['sku']."' type='checkbox' class='checkbox'>
                  <p>BOOK-".$output['sku']."</p>
                  <p>".$output['title']."</p>
                  <p>$".$output['price']."</p>
                  <p>".$output['wght']." KG</p>
                </div>
              ";
              }
              } else {
                  echo "Nothing was found";
              }
            }

            function printFurniture($conn,$request){
                $result = $conn->query($request);
                if(checkRow($result)){
                while($output = $result->fetch_assoc()){
                echo "
                  <div class='product_block'>
                    <input id='FUR-".$output['sku']."' type='checkbox' class='checkbox'>    
                    <p>FUR-".$output['sku']."</p>
                    <p>".$output['title']."</p>
                    <p>$".$output['price']."</p>
                    <p>".$output['dem']."</p>
                  </div>
                ";
                }
              } else {
                  echo "Nothing was found";
              }
            }
        ?>